# Infra GCP Schaakclublievegem

## Terraform state management

[Using Terraform Cloud as the Backend](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#using-terraform-cloud-as-the-backend)

TLDR:
1. Create env var GOOGLE_CREDENTIALS in TF Cloud's workspace
2. Remove newline chars from the JSON key file, then paste the env var
3. Mark the var as **Sensitive** and click Save
