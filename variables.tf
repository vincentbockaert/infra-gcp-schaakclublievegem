variable "vpc_network_name" {
  type = string
  default = "terraform-network-production"
}