# Create a single Compute Engine instance

provider "google" {
  project = "schaakclublievegem"
}

resource "google_compute_instance" "default" {
  name         = "express-site"
  machine_type = "f1-micro"
  zone         = "europe-west1-d"
  tags         = ["ssh"]

  metadata = {
    enable-oslogin = "TRUE"
  }
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  # startup script, include podman for app podman image
  # also sets the default container registries to check, first one gets checked then next and so on
  # https://www.redhat.com/sysadmin/manage-container-registries
  metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq podman; echo \"unqualified-search-registries=['grc.io','docker.io', 'quay.io']\" > $HOME/.config/containers/registries.conf"

  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

resource "google_compute_firewall" "ssh" {
  name = "allow-ssh"
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.self_link
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}

# prevent using the "default" network which every GCP Project ships with
resource "google_compute_network" "vpc_network" {
  name                    = var.vpc_network_name
  auto_create_subnetworks = "true"
}
